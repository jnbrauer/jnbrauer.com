locals {
  env         = terraform.workspace == "jnbrauer-com-test" ? "test" : "prod"
  base_domain = "jnbrauer.com"
  domain      = local.env == "test" ? "test.${local.base_domain}" : local.base_domain
  dev_domain  = local.env == "test" ? "test.jnbrauer.dev" : "jnbrauer.dev"
}

resource "aws_s3_bucket" "website_bucket" {
  bucket = local.domain
  acl    = "public-read"
  policy = templatefile("bucket_policy.json", { domain = local.domain })

  cors_rule {
    allowed_headers = ["Content-Length"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
  }

  website {
    index_document = "index.html"
  }
}

data "aws_acm_certificate" "ssl_cert" {
  domain = local.base_domain
}

resource "aws_cloudfront_distribution" "cloudfront_dist" {
  origin {
    domain_name = aws_s3_bucket.website_bucket.website_endpoint
    origin_id   = local.domain

    custom_origin_config {
      http_port  = 80
      https_port = 443

      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1"]
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  aliases = [local.domain, local.dev_domain]

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods  = ["GET", "HEAD"]

    target_origin_id = local.domain

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
  }

  viewer_certificate {
    acm_certificate_arn = data.aws_acm_certificate.ssl_cert.arn
    ssl_support_method  = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}

data "aws_route53_zone" "main_zone" {
  name = local.base_domain
}

resource "aws_route53_record" "record" {
  zone_id = data.aws_route53_zone.main_zone.zone_id
  name    = local.domain
  type    = "A"

  alias {
    name    = aws_cloudfront_distribution.cloudfront_dist.domain_name
    zone_id = aws_cloudfront_distribution.cloudfront_dist.hosted_zone_id

    evaluate_target_health = false
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  cloud {
    organization = "jnbrauer"

    workspaces {
      tags = ["jnbrauer-com"]
    }
  }
}

provider "aws" {
  region = "us-east-1"
}
